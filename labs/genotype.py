# genes is a list of -1 and 1
class LabsGenotype(object):
    def __init__(self, genes, min_fitness=0, max_fitness=0):
        super(LabsGenotype, self).__init__()
        self.fitness = None
        self.genes = genes
        # Parametry potrzebne przy COMMA
        self.min_fitness = min_fitness
        self.max_fitness = max_fitness

    def __str__(self):
        return "%s, f:%s" % (self.genes, self.fitness)

    def __repr__(self):
        return self.__str__()
