import random
from pyage.core.operator import Operator
from pyage.solutions.evolution.crossover import AbstractCrossover
from pyage.solutions.evolution.genotype import PointGenotype, FloatGenotype
from labs.genotype import LabsGenotype
from labs.initializer import LabsInitializer

class NoOpCrossover(AbstractCrossover):
    def __init__(self, size=100):
        super(NoOpCrossover, self).__init__(LabsGenotype, size)

    def cross(self, p1, p2):
        if p1.fitness > p2.fitness:
            return LabsGenotype(p1.genes, p1.min_fitness, p1.max_fitness)
        else:
            return LabsGenotype(p2.genes, p2.min_fitness, p2.max_fitness) 

# Krzyzowanie nic nie robi, zwraca losowy z genotypow
class NoOpRandomCrossover(AbstractCrossover):
    def __init__(self, size=100):
        super(NoOpRandomCrossover, self).__init__(LabsGenotype, size)

    def cross(self, p1, p2):
        if random.randint(0,1) == 0:
            return LabsGenotype(p1.genes)
        else:
            return LabsGenotype(p2.genes)

# Krzyzownaie jednopunktowe dla LABS
class LabsSinglePointCrossover(AbstractCrossover):
    def __init__(self, size=100):
        super(LabsSinglePointCrossover, self).__init__(LabsGenotype, size)

    def cross(self, p1, p2):
        crossingPoint = random.randint(1, len(p1.genes))
        return LabsGenotype(p1.genes[:crossingPoint] + p2.genes[crossingPoint:], max_fitness = p1.max_fitness, min_fitness = p1.min_fitness)
