from math import cos, pi, sin, sqrt
from numba import cuda, jit, autojit
import numpy
from pyage.core.operator import Operator
from pyage.solutions.evolution.genotype import PointGenotype, FloatGenotype


# @cuda.jit
@autojit(target='gpu')
def merit_factor(genes_matrix, results_vector):
    genotype_number = cuda.grid(1)
    genes_count = genes_matrix.shape[1]

    E = 0
    L = genes_count
    for k in xrange(1, L):
        Ck = 0
        for i in xrange(L - k - 1):
            Ck += genes_matrix[genotype_number, i] * genes_matrix[genotype_number, i + k]
        E += (Ck) ** 2
    #return -E
    results_vector[genotype_number] = L ** 2 / (2. * E)


# Wylicza fitness genotypu za pomoca wskaznika zwanego merit factor
class LabsEvaluation(Operator):
    def __init__(self):
        super(LabsEvaluation, self).__init__()

    def process(self, population):

        blocks_per_grid = len(population)
        threads_per_block = 1

        genes_matrix = numpy.array([genotype.genes for genotype in population])
        results_vector = numpy.zeros(len(population))

        merit_factor[blocks_per_grid, threads_per_block](genes_matrix, results_vector)

        for i in range(len(population)):
            population[i].fitness = results_vector[i]

    def process_via_cpu(self, population):
        for genotype in population:
            genotype.fitness = self.merit_factor(genotype.genes)

    # f(S) = L^2 / (2 * E(S))
    # E(S) = sum[k = 1 to L-k]((C[k](S))^2)
    # C(S) = sum[i = 1 to k](s[i]s[i+k])
    def merit_factor(self, genes):
        E = 0
        L = len(genes)
        for k in xrange(1, L):
            Ck = 0
            for i in xrange(L - k - 1):
                Ck += genes[i] * genes[i + k]
            E += (Ck) ** 2
        #return -E
        return L ** 2 / (2. * E)

