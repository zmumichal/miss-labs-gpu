import random
import math
from pyage.core.operator import Operator
from pyage.solutions.evolution.genotype import PointGenotype, FloatGenotype
from pyage.solutions.evolution.mutation import AbstractMutation
from labs.genotype import LabsGenotype
from labs.gnuplot import LabsStatistics
from labs.evaluation import LabsEvaluation

class UniformLabsMutation(AbstractMutation):
    def __init__(self, probability=1):
        super(UniformLabsMutation, self).__init__(LabsGenotype, probability)


    def mutate(self, genotype):
        index = random.randint(0, len(genotype.genes) - 1)
        genotype.genes[index] = -genotype.genes[index]

# Trzeba bedzie znalezc gdzie bedzie trzeba podac sile mutacji, bo raczej nie przy inicie, ale przy mutacji, co jest troche problematyczne
# Albo trzeba tutaj wyliczac sile ale do tego jest potrzebne wiecej informacji
# moze mozna te informacje trzymac w genotypie i wyliczas przy okazji evaluation?
class UniformLabsMutationWithStrength(AbstractMutation):
    def __init__(self, probability=1, bad_mutation_probability=0.2, min_strength=1, max_strength=3):
        super(UniformLabsMutationWithStrength, self).__init__(LabsGenotype, probability)
        self.bad_mutation_probability = bad_mutation_probability # szansa, ze gorszy genotyp zastapi lepszy
        self.min_strength = min_strength
        self.max_strength = max_strength


    def mutate(self, genotype):
        # Fitness aktualnego rozwiazania
        fitness = LabsEvaluation().merit_factor(genotype.genes)
        # Kopia genow
        new_genes = genotype.genes[:]
        index = random.randint(0, len(genotype.genes) - 1)
        newindex = index
        for i in xrange(self.get_mutation_power(fitness, genotype.min_fitness, genotype.max_fitness)):
            new_genes[newindex] = - new_genes[newindex]
            newindex = (newindex + 1) % len(new_genes) 
            # mozna rozwazyc nie przejscie na poczatek ale pojscie w lewa strone 
            # od wylosowanego na poczatku indeksu ale niekoniecznie
            # TODO mozna pomyslec o zupelnym losowaniu albo o czyms innym

        # Fitness nowego rozwiazania
        new_fitness = LabsEvaluation().merit_factor(new_genes)
        if (new_fitness > fitness): 
            # Jesli lepszy genotyp, podmien
            genotype.genes = new_genes[:]
        elif (random.uniform(0.0, 1.0) < self.bad_mutation_probability):
            # Jesli gorszy, podmien z szansa = bad_mutation_probability
            genotype.genes = new_genes[:]


    # Znamy max fitness, mozna przeskalowac na sile mutacji (pole statyczne w LabsStatistics)
    # Zakladamy, ze minimalny fitness w populacji wynosi 0
    # Skalujemy liniowo na sile mutacji z przedzialu (min_str;max_str)
    def get_mutation_power(self, fitness, min_fitness, max_fitness):
        if (max_fitness == min_fitness):
            if LabsStatistics.max_fitness == 0:
                bias = 0
            else:
                bias = fitness / LabsStatistics.max_fitness
        else:
            bias = (fitness - min_fitness) / (max_fitness - min_fitness)
        mutation_power = self.max_strength - \
            math.floor((self.max_strength - self.min_strength + 1) * bias)
        return int(mutation_power)

# Mutacja dla Labs COMMA wielopunktowa
class MultiUniformLabsMutationWithStrength(AbstractMutation):
    def __init__(self, probability=1, bad_mutation_probability=0.2, min_strength=1, max_strength=3, multi = 2):
        super(MultiUniformLabsMutationWithStrength, self).__init__(LabsGenotype, probability)
        self.bad_mutation_probability = bad_mutation_probability # szansa, ze gorszy genotyp zastapi lepszy
        self.min_strength = min_strength
        self.max_strength = max_strength
        self.multi = multi


    def mutate(self, genotype):
        # Fitness aktualnego rozwiazania
        fitness = LabsEvaluation().merit_factor(genotype.genes)
        # Kopia genow
        new_genes = genotype.genes[:]
        # Mutacja z odpowiednia sila 
        for j in xrange(self.multi):
            index = random.randint(0, len(genotype.genes) - 1)
            newindex = index
            for i in xrange(self.get_mutation_power(fitness, genotype.min_fitness, genotype.max_fitness)):
                new_genes[newindex] = - new_genes[newindex]
                newindex = (newindex + 1) % len(new_genes) 
                

        # Fitness nowego rozwiazania
        new_fitness = LabsEvaluation().merit_factor(new_genes)
        if (new_fitness > fitness): 
            # Jesli lepszy genotyp, podmien
            genotype.genes = new_genes[:]
        elif (random.uniform(0.0, 1.0) < self.bad_mutation_probability):
            # Jesli gorszy, podmien z szansa = bad_mutation_probability
            genotype.genes = new_genes[:]


    # Znamy max fitness, mozna przeskalowac na sile mutacji (pole statyczne w LabsStatistics)
    # Zakladamy, ze minimalny fitness w populacji wynosi 0
    # Skalujemy liniowo na sile mutacji z przedzialu (min_str;max_str)
    def get_mutation_power(self, fitness, min_fitness, max_fitness):
        if (max_fitness == min_fitness):
            if LabsStatistics.max_fitness == 0:
                bias = 0
            else:
                bias = fitness / LabsStatistics.max_fitness
        else:
            bias = (fitness - min_fitness) / (max_fitness - min_fitness)
        mutation_power = self.max_strength - \
            math.floor((self.max_strength - self.min_strength + 1) * bias)
        return int(mutation_power)


# Mutacja dla Labs COMMA wielopunktowa
class RandomLabsMutationWithStrength(AbstractMutation):
    def __init__(self, probability=1, bad_mutation_probability=0.2, min_strength=1, max_strength=3):
        super(RandomLabsMutationWithStrength, self).__init__(LabsGenotype, probability)
        self.bad_mutation_probability = bad_mutation_probability # szansa, ze gorszy genotyp zastapi lepszy
        self.min_strength = min_strength
        self.max_strength = max_strength


    def mutate(self, genotype):
        # Fitness aktualnego rozwiazania
        fitness = LabsEvaluation().merit_factor(genotype.genes)
        # Kopia genow
        new_genes = genotype.genes[:]
        # Mutacja z odpowiednia sila 
        for i in xrange(self.get_mutation_power(fitness, genotype.min_fitness, genotype.max_fitness)):
            index = random.randint(0, len(genotype.genes) - 1)
            new_genes[index] = - new_genes[index]
                

        # Fitness nowego rozwiazania
        new_fitness = LabsEvaluation().merit_factor(new_genes)
        if (new_fitness > fitness): 
            # Jesli lepszy genotyp, podmien
            genotype.genes = new_genes[:]
        elif (random.uniform(0.0, 1.0) < self.bad_mutation_probability):
            # Jesli gorszy, podmien z szansa = bad_mutation_probability
            genotype.genes = new_genes[:]


    # Znamy max fitness, mozna przeskalowac na sile mutacji (pole statyczne w LabsStatistics)
    # Zakladamy, ze minimalny fitness w populacji wynosi 0
    # Skalujemy liniowo na sile mutacji z przedzialu (min_str;max_str)
    def get_mutation_power(self, fitness, min_fitness, max_fitness):
        if (max_fitness == min_fitness):
            if LabsStatistics.max_fitness == 0:
                bias = 0
            else:
                bias = fitness / LabsStatistics.max_fitness
        else:
            bias = (fitness - min_fitness) / (max_fitness - min_fitness)
        mutation_power = self.max_strength - \
            math.floor((self.max_strength - self.min_strength + 1) * bias)
        return int(mutation_power)



# Mutacja dla Labs COMMA wielopunktowa, memetyczna (?)
class MemeticRandomLabsMutationWithStrength(AbstractMutation):
    def __init__(self, probability=1, bad_mutation_probability=0.2, min_strength=1, max_strength=3, tree_width=3, tree_height=3):
        super(MemeticRandomLabsMutationWithStrength, self).__init__(LabsGenotype, probability)
        self.bad_mutation_probability = bad_mutation_probability # szansa, ze gorszy genotyp zastapi lepszy
        self.min_strength = min_strength
        self.max_strength = max_strength
        self.tree_width = tree_width
        self.tree_height = tree_height


    def mutate(self, genotype):
        # Fitness aktualnego rozwiazania
        fitness = LabsEvaluation().merit_factor(genotype.genes)

        # sila mutacji zalezna od pozycji w populacji
        mutation_power = self.get_mutation_power(fitness, genotype.min_fitness, genotype.max_fitness)

        # najlepsza mutacja globalnie
        best_genes = genotype.genes
        best_fitness = fitness

        # genotyp mutowany na tym poziomie
        current_genes = genotype.genes

        # Mutacja memetyczna - genotyp mutowany jest kilkukrotnie, wybierany jest najlepszy genotyp
        # Ten najlepszy znow jest mutowany kilkukrotnie, i tak przez kilka poziomow. Bierzemy najlepszy genotyp.
        for i in xrange(self.tree_height):
            # najlepsza mutacja na danym poziomie
            current_best_genes = current_genes
            current_best_fitness = 0
            for j in xrange(self.tree_width):    
                # Kopia genow
                new_genes = current_genes[:]
                # Mutacja z odpowiednia sila 
                for k in xrange(mutation_power):
                    index = random.randint(0, len(genotype.genes) - 1)
                    new_genes[index] = - new_genes[index]

                # Sprawdzenie nowego genotypu
                new_fitness = LabsEvaluation().merit_factor(new_genes)
                if new_fitness > current_best_fitness:
                    current_best_genes = new_genes
                    current_best_fitness = new_fitness

                if new_fitness > best_fitness:
                    best_genes = new_genes
                    best_fitness = new_fitness

            current_genes = current_best_genes                

        if (best_fitness > fitness): 
            # Jesli lepszy genotyp, podmien
            genotype.genes = best_genes[:]
        elif (random.uniform(0.0, 1.0) < self.bad_mutation_probability):
            # Jesli gorszy, podmien z szansa = bad_mutation_probability
            genotype.genes = best_genes[:]


    # Znamy max fitness, mozna przeskalowac na sile mutacji (pole statyczne w LabsStatistics)
    # Zakladamy, ze minimalny fitness w populacji wynosi 0
    # Skalujemy liniowo na sile mutacji z przedzialu (min_str;max_str)
    def get_mutation_power(self, fitness, min_fitness, max_fitness):
        if (max_fitness == min_fitness):
            if LabsStatistics.max_fitness == 0:
                bias = 0
            else:
                bias = fitness / LabsStatistics.max_fitness
        else:
            bias = (fitness - min_fitness) / (max_fitness - min_fitness)
        mutation_power = self.max_strength - \
            math.floor((self.max_strength - self.min_strength + 1) * bias)
        return int(mutation_power)
